package com.motivator.bilkanalen.prozess;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProzessApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProzessApplication.class, args);
	}

}

