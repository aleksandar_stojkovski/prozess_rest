package com.motivator.bilkanalen.prozess.repository;

import com.motivator.bilkanalen.prozess.model.Contact;
import com.motivator.bilkanalen.prozess.model.WeeklySaleRecord;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Date;
import java.util.List;

/**
 * Created by Aleksandar on Dec, 2018
 */
public interface WeeklySaleRecordRepository extends JpaRepository<WeeklySaleRecord, Integer> {

    List<WeeklySaleRecord> findBySellerIdAndSaleDateBetween(int id, Date start, Date end);
    List<WeeklySaleRecord> findBySellerIdAndSaleDateBetweenAndHandler(int id, Date start, Date end, String handler);


}
