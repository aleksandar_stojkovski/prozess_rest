package com.motivator.bilkanalen.prozess.repository;

import com.motivator.bilkanalen.prozess.model.Domain;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 * Created by Aleksandar on Dec, 2018
 */
public interface DomainRepository extends JpaRepository<Domain, Integer> {

    List<Domain> findAll();
    Domain findById(int id);
}
