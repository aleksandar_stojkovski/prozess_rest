package com.motivator.bilkanalen.prozess.repository;

import com.motivator.bilkanalen.prozess.model.Contact;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface ContactRepository extends JpaRepository<Contact, Integer> {

    Contact findByUsername(String username);
    Contact findById(int id);

    @Query("SELECT c FROM Contact c WHERE LOWER(c.name) LIKE CONCAT ('%',LOWER(:fieldName),'%') " +
            "AND c.domain.id = :domain " +
            "AND (c.securityLevel=3 OR c.securityLevel=7)")
    List<Contact> findByBasicFields(@Param("fieldName")String fieldName, @Param("domain") int domain);

}
