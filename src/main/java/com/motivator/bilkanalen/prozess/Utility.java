package com.motivator.bilkanalen.prozess;

import com.motivator.bilkanalen.prozess.model.Contact;
import com.motivator.bilkanalen.prozess.model.SecurityLevel;
import org.springframework.security.core.context.SecurityContextHolder;

import javax.naming.AuthenticationException;

/**
 * Created by Aleksandar on Dec, 2018
 */
public class Utility {

    public static void errorIfUSerIsNotInRole(Contact contact, SecurityLevel... levels) throws AuthenticationException {
        boolean inRole = false;
        for (SecurityLevel role : levels) {
            inRole = inRole || role.equals(contact.getSecurityLevel());
        }
        if (!inRole) {
            throw new AuthenticationException("Current user is not in any of the required roles");
        }
    }

    public static Contact getPrincipal() throws Exception {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (principal == null) {
            throw new Exception("Contact not found");
        }
        return (Contact) principal;
    }
}
