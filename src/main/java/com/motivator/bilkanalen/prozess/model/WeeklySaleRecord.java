package com.motivator.bilkanalen.prozess.model;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Aleksandar on Dec, 2018
 */
@Entity
@Table(name = "weeklysalerecord")
public class WeeklySaleRecord {

    @Id
    private Integer id;
    @Column(name = "carbrand")
    private String carBrand;
    private String model;
    @Column(name = "chassisnumber")
    private String chassisNumber;
    @Column(name = "handlerresponsible")
    private String handler;

    @Column(name = "saledate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date saleDate;
    @ManyToOne
    @JoinColumn(name = "seller_id")
    private Contact seller;

    public WeeklySaleRecord() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCarBrand() {
        return carBrand;
    }

    public void setCarBrand(String carBrand) {
        this.carBrand = carBrand;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getChassisNumber() {
        return chassisNumber;
    }

    public void setChassisNumber(String chassisNumber) {
        this.chassisNumber = chassisNumber;
    }

    public String getHandler() {
        return handler;
    }

    public void setHandler(String handler) {
        this.handler = handler;
    }

    public Contact getSeller() {
        return seller;
    }

    public void setSeller(Contact seller) {
        this.seller = seller;
    }

    public Date getSaleDate() {
        return saleDate;
    }

    public void setSaleDate(Date saleDate) {
        this.saleDate = saleDate;
    }

    @Override
    public String toString() {
        return "WeeklySaleRecord{" +
                "id=" + id +
                ", carBrand='" + carBrand + '\'' +
                ", model='" + model + '\'' +
                ", chassisNumber='" + chassisNumber + '\'' +
                ", handler='" + handler + '\'' +
                ", seller=" + seller +
                '}';
    }
}
