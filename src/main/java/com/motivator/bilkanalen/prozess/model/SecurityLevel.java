package com.motivator.bilkanalen.prozess.model;

public enum SecurityLevel {

    None, Install, Manager, Employee, Participant, Customer, Admin, Partner, HandlerLeader, Viewer;

    public String toLocale(){
        return "ROLE_" + name().toUpperCase();
    }
    public static String[] getUserLevels() {
        return new String[]{
                        None.name().toUpperCase(), Install.name().toUpperCase(),
                        Manager.name().toUpperCase(), Employee.name().toUpperCase(), Customer.name().toUpperCase(),
                        Participant.name().toUpperCase(), Admin.name().toUpperCase(), Partner.name().toUpperCase(),
                        HandlerLeader.name().toUpperCase(), Viewer.name().toUpperCase()
        };
    }
}
