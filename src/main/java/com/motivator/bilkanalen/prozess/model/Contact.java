package com.motivator.bilkanalen.prozess.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlElement;
import java.util.Date;

@Entity
public class Contact {

    @Id
    private Integer id;
    @Column(name = "contactfirstname")
    private String firstName;
    @Column(name = "contactlastname")
    private String lastName;

    @Column(name = "fileas")
    private String name;
    private String username;
    private String password;

    @NotNull
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date created;

    @Enumerated(EnumType.ORDINAL)
    @XmlElement
    @Column(name = "partnersrole")
    private PartnersRole partnersRole = PartnersRole.None;

    @Enumerated
    @XmlElement
    @Column(name = "securitylevel")
    private SecurityLevel securityLevel;

    @ManyToOne
    @JoinColumn(name = "domain_id")
    private Domain domain;

    public Integer getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public PartnersRole getPartnersRole() {
        return partnersRole;
    }

    public void setPartnersRole(PartnersRole partnersRole) {
        this.partnersRole = partnersRole;
    }

    public SecurityLevel getSecurityLevel() {
        return securityLevel;
    }

    public void setSecurityLevel(SecurityLevel securityLevel) {
        this.securityLevel = securityLevel;
    }

    public Domain getDomain() {
        return domain;
    }

    public void setDomain(Domain domain) {
        this.domain = domain;
    }

    @Override
    public String toString() {
        return "Contact{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", created=" + created +
                ", partnersRole=" + partnersRole +
                ", securityLevel=" + securityLevel +
                '}';
    }
}
