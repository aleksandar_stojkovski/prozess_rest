package com.motivator.bilkanalen.prozess.model;

import javax.persistence.*;
import java.util.List;

/**
 * Created by Aleksandar on Dec, 2018
 */

@Entity
public class Domain {

    @Id
    private int id;
    private String title;

    public Domain() {
    }

    public Domain(int id, String title) {
        this.id = id;
        this.title = title;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
