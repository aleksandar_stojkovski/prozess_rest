package com.motivator.bilkanalen.prozess.model;

public enum PartnersRole {

    None, Seller, Secretary, Leader, LeaderAndSeller, SellerHandler, HandlerLeader, ContactCenter;

    public String toLocale() {
        return "ROLE_" + name().toUpperCase();
    }

    public static String[] getRoles(){
        return new String[]{None.name().toUpperCase(), Seller.name().toUpperCase(), Secretary.name().toUpperCase(),
                Leader.name().toUpperCase(), LeaderAndSeller.name().toUpperCase(), SellerHandler.name().toUpperCase(),
                HandlerLeader.name().toUpperCase(), ContactCenter.name().toUpperCase()
        };
    }
}
