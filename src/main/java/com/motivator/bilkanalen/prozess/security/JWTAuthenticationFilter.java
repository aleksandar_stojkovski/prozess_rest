package com.motivator.bilkanalen.prozess.security;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.motivator.bilkanalen.prozess.model.Contact;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Date;

import static com.motivator.bilkanalen.prozess.security.SecurityConstants.*;

public class JWTAuthenticationFilter extends UsernamePasswordAuthenticationFilter {

    private AuthenticationManager authenticationManager;

    public JWTAuthenticationFilter(AuthenticationManager authenticationManager) {
        this.authenticationManager = authenticationManager;
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) throws AuthenticationException {
        try{
            Contact contact = new ObjectMapper().readValue(request.getInputStream(), Contact.class);
            String encryptedPassword = cryptWithMD5(contact.getPassword());
            return authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(contact.getUsername(), encryptedPassword));
        }catch (IOException e){
            throw new RuntimeException(e);
        }
    }

    @Override
    protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain, Authentication authResult) throws IOException, ServletException {
        ZonedDateTime zonedDateTime = ZonedDateTime.now(ZoneOffset.UTC).plus(EXPIRE_TIME, ChronoUnit.MILLIS);

        String token = Jwts.builder()
                .setSubject(((User)authResult.getPrincipal()).getUsername())
                .setExpiration(Date.from(zonedDateTime.toInstant()))
                .signWith(SignatureAlgorithm.HS256, SECRET)
                .compact();
        System.out.println("Token: " + token);
        response.getWriter().write(token);
        response.addHeader(HEADER_STRING, TOKEN_PREFIX + token);
    }

    public String cryptWithMD5(String password){
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-256"); // Encryption algorithm
            byte[] output = md.digest(password.getBytes(StandardCharsets.UTF_8));
            return new String(Base64.encodeBase64(output));
        }
        catch (Exception e) {
            return null;
        }
    }
}
