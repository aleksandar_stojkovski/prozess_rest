package com.motivator.bilkanalen.prozess.security;

import com.motivator.bilkanalen.prozess.model.SecurityLevel;
import com.motivator.bilkanalen.prozess.service.CustomUserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    private CustomUserDetailsService customUserDetailsService;

    @Autowired
    public SecurityConfig(CustomUserDetailsService customUserDetailsService) {
        this.customUserDetailsService = customUserDetailsService;
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.cors().and().csrf().disable().authorizeRequests()
                .antMatchers("/**").hasAnyRole(SecurityLevel.getUserLevels())
                .and()
                .addFilter(new JWTAuthorizationFilter(authenticationManager(), customUserDetailsService))
                .addFilter(new JWTAuthenticationFilter(authenticationManager()));

    }
}
