package com.motivator.bilkanalen.prozess.controller;

import com.motivator.bilkanalen.prozess.model.Contact;
import com.motivator.bilkanalen.prozess.model.WeeklySaleRecord;
import com.motivator.bilkanalen.prozess.service.ContactService;
import com.motivator.bilkanalen.prozess.service.WeeklySaleRecordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by Aleksandar on Dec, 2018
 */

@RestController
@RequestMapping("/sales")
public class WeeklySaleRecordController {

    private WeeklySaleRecordService weeklySaleRecordService;
    private ContactService contactService;

    @Autowired
    public WeeklySaleRecordController(WeeklySaleRecordService weeklySaleRecordService, ContactService contactService) {
        this.weeklySaleRecordService = weeklySaleRecordService;
        this.contactService = contactService;
    }

    @GetMapping("{id}")
    public List<WeeklySaleRecord> findBySellerIdAndSaleDate(@PathVariable("id") int id, @RequestParam("isHandler") boolean isHandler) throws Exception {
        if(!isHandler){
            return weeklySaleRecordService.findBySellerIdAndSaleDate(id);
        }
        return weeklySaleRecordService.findBySellerIdAndSaleDateAndHandler(id);
    }
}
