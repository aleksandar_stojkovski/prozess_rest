package com.motivator.bilkanalen.prozess.controller;

import com.motivator.bilkanalen.prozess.model.Domain;
import com.motivator.bilkanalen.prozess.service.DomainService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by Aleksandar on Dec, 2018
 */

@RestController
@RequestMapping("domains")
public class DomainController {

    private DomainService domainService;

    @Autowired
    public DomainController(DomainService domainService) {
        this.domainService = domainService;
    }

    @GetMapping("")
    public List<Domain> findAll(){
        return domainService.findAll();
    }

    @GetMapping("id")
    public Domain findById(@RequestParam("id") int id){
        return domainService.findById(id);
    }
}
