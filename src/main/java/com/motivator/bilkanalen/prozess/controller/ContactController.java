package com.motivator.bilkanalen.prozess.controller;

import com.motivator.bilkanalen.prozess.model.Contact;
import com.motivator.bilkanalen.prozess.service.ContactService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/contacts")
public class ContactController {

    private ContactService contactService;

    @Autowired
    public ContactController(ContactService contactService) {
        this.contactService = contactService;
    }

    @GetMapping("/me")
    public Contact getMe() throws Exception {
        return contactService.getMe();
    }

    @GetMapping("{id}")
    public Contact findById(@PathVariable int id) throws Exception {
        return contactService.findById(id);
    }

    @GetMapping("")
    public List<Contact> findByBasicFields(@RequestParam("fieldName") String fieldName) throws Exception {
        System.out.println("Size: " + contactService.findByBasicFields(fieldName).size());
        return contactService.findByBasicFields(fieldName);
    }
}
