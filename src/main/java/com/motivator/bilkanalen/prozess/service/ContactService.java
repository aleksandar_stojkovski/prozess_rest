package com.motivator.bilkanalen.prozess.service;

import com.motivator.bilkanalen.prozess.model.Contact;

import java.util.List;

/**
 * Created by Aleksandar on Dec, 2018
 */
public interface ContactService {

    Contact getMe() throws Exception;
    Contact findById(int id) throws Exception;
    List<Contact> findByBasicFields(String fieldName) throws Exception;
}
