package com.motivator.bilkanalen.prozess.service.implementation;

import com.motivator.bilkanalen.prozess.Utility;
import com.motivator.bilkanalen.prozess.model.Contact;
import com.motivator.bilkanalen.prozess.model.WeeklySaleRecord;
import com.motivator.bilkanalen.prozess.repository.WeeklySaleRecordRepository;
import com.motivator.bilkanalen.prozess.service.ContactService;
import com.motivator.bilkanalen.prozess.service.WeeklySaleRecordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by Aleksandar on Dec, 2018
 */

@Service
public class WeeklySaleRecordServiceImpl implements WeeklySaleRecordService {

    private WeeklySaleRecordRepository weeklySaleRecordRepository;
    private ContactService contactService;

    @Autowired
    public WeeklySaleRecordServiceImpl(WeeklySaleRecordRepository weeklySaleRecordRepository,ContactService contactService) {
        this.weeklySaleRecordRepository = weeklySaleRecordRepository;
        this.contactService = contactService;
    }

    @Override
    public List<WeeklySaleRecord> findBySellerIdAndSaleDate(int id) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.MONTH, 0);
        calendar.set(Calendar.DAY_OF_YEAR, 0);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        Date start = calendar.getTime();
        Date end = new Date();
        return weeklySaleRecordRepository.findBySellerIdAndSaleDateBetween(id, start, end);
    }

    @Override
    public List<WeeklySaleRecord> findBySellerIdAndSaleDateAndHandler(int id) throws Exception {

        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.MONTH, 0);
        calendar.set(Calendar.DAY_OF_YEAR, 0);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        Date start = calendar.getTime();
        Date end = new Date();
        Contact me = Utility.getPrincipal();
        return weeklySaleRecordRepository.findBySellerIdAndSaleDateBetweenAndHandler(id, start, end, me.getName());
    }
}
