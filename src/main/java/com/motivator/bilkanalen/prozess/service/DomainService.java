package com.motivator.bilkanalen.prozess.service;

import com.motivator.bilkanalen.prozess.model.Domain;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Aleksandar on Dec, 2018
 */

public interface DomainService {

    List<Domain> findAll();
    Domain findById(int id);
}
