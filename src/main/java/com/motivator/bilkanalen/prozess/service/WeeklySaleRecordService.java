package com.motivator.bilkanalen.prozess.service;

import com.motivator.bilkanalen.prozess.model.Contact;
import com.motivator.bilkanalen.prozess.model.WeeklySaleRecord;

import java.util.Date;
import java.util.List;

/**
 * Created by Aleksandar on Dec, 2018
 */
public interface WeeklySaleRecordService {

    List<WeeklySaleRecord> findBySellerIdAndSaleDate(int id);
    List<WeeklySaleRecord> findBySellerIdAndSaleDateAndHandler(int id) throws Exception;
}
