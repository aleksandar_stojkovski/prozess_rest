package com.motivator.bilkanalen.prozess.service.implementation;

import com.motivator.bilkanalen.prozess.model.Domain;
import com.motivator.bilkanalen.prozess.repository.DomainRepository;
import com.motivator.bilkanalen.prozess.service.DomainService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Aleksandar on Dec, 2018
 */

@Service
public class DomainServiceImpl implements DomainService {

    private DomainRepository domainRepository;

    @Autowired
    public DomainServiceImpl(DomainRepository domainRepository) {
        this.domainRepository = domainRepository;
    }

    @Override
    public List<Domain> findAll() {
        return domainRepository.findAll();
    }

    @Override
    public Domain findById(int id) {
        return domainRepository.findById(id);
    }
}
