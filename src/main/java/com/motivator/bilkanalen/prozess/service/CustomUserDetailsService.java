package com.motivator.bilkanalen.prozess.service;

import com.motivator.bilkanalen.prozess.model.Contact;
import com.motivator.bilkanalen.prozess.model.PartnersRole;
import com.motivator.bilkanalen.prozess.repository.ContactRepository;
import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.Collections;

@Component
public class CustomUserDetailsService implements UserDetailsService {

    private ContactRepository contactRepository;

    @Autowired
    public CustomUserDetailsService(ContactRepository contactRepository) {
        this.contactRepository = contactRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        Contact contact = contactRepository.findByUsername(s);
        return new User(contact.getUsername(), passwordEncoder().encode(contact.getPassword()), Collections.singletonList(new SimpleGrantedAuthority(contact.getSecurityLevel().toLocale())));
    }

    public Contact loadContactByUsername(String username){
        return contactRepository.findByUsername(username);
    }

    @Bean
    public BCryptPasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

}
