package com.motivator.bilkanalen.prozess.service.implementation;

import com.motivator.bilkanalen.prozess.Utility;
import com.motivator.bilkanalen.prozess.model.Contact;
import com.motivator.bilkanalen.prozess.model.Domain;
import com.motivator.bilkanalen.prozess.model.SecurityLevel;
import com.motivator.bilkanalen.prozess.repository.ContactRepository;
import com.motivator.bilkanalen.prozess.service.ContactService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.naming.AuthenticationException;
import javax.xml.ws.WebServiceContext;
import java.util.List;

/**
 * Created by Aleksandar on Dec, 2018
 */

@Service
public class ContactServiceImpl implements ContactService {

    private ContactRepository contactRepository;

    @Resource
    private WebServiceContext securityContext;

    @Autowired
    public ContactServiceImpl(ContactRepository contactRepository) {
        this.contactRepository = contactRepository;
    }

    @Override
    public Contact getMe() throws Exception {
        Contact me = Utility.getPrincipal();
        Utility.errorIfUSerIsNotInRole(me, SecurityLevel.Partner, SecurityLevel.Admin, SecurityLevel.Employee, SecurityLevel.Manager);
        String username;
        username = me.getUsername();
        if (username == null) {
            throw new RuntimeException("Username does not exist");
        }
        return contactRepository.findByUsername(username);
    }

    @Override
    public Contact findById(int id) throws Exception {
        Contact me = Utility.getPrincipal();
        Utility.errorIfUSerIsNotInRole(me, SecurityLevel.Manager, SecurityLevel.Admin);
        return contactRepository.findById(id);
    }

    @Override
    public List<Contact> findByBasicFields(String fieldName) throws Exception {
        Contact me = Utility.getPrincipal();
        Domain domain = me.getDomain();
        Utility.errorIfUSerIsNotInRole(me, SecurityLevel.Manager, SecurityLevel.Admin, SecurityLevel.Partner, SecurityLevel.Employee);
        return contactRepository.findByBasicFields(fieldName, domain.getId());
    }


}
